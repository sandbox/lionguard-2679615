INTRODUCTION
------------
The Lob.com Printing API module is a Drupal integration for the Lob.com Printing API.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/lionguard/2679615

 * For more information about HelloSign and its features:
   https://www.lob.com/


REQUIREMENTS
------------
This module requires the following library:
 * lob-php (https://github.com/lob/lob-php)


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


 * Go to "sites/all/libraries" and create lob folder, inside this folder
   run curl -sS https://getcomposer.org/installer | php to install composer an then
   run php composer.phar require lob/lob-php:1.4.* , path should be
   /sites/lob/sites/all/libraries/lob/vendor/autoload.php



CONFIGURATION
-------------


USING THE API
-------------

MAINTAINERS
-----------
Current maintainers:
 * Igor Taldenko (https://www.drupal.org/u/italdenko)
