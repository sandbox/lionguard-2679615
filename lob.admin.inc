<?php
/**
 * @file
 * Provide forms and submit functions for lob.com.
 */


/**
 * Generates the lob type editing form.
 */
function lob_type_form($form, &$form_state, $lob_type, $op = 'edit') {

  if ($op == 'clone') {
    $lob_type->label .= ' (cloned)';
    $lob_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $lob_type->label,
    '#description' => t('The human-readable name of this lob type.'),
    '#required' => TRUE,
    '#size' => 30,
  );

  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($lob_type->type) ? $lob_type->type : '',
    '#maxlength' => 32,
    '#disabled' => $lob_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'lob_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this lob type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($lob_type->description) ? $lob_type->description : '',
    '#description' => t('Description about the lob type.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save lob type'),
    '#weight' => 40,
  );

  if (!$lob_type->isLocked() && $op != 'add' && $op != 'clone') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete lob type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('lob_type_form_submit_delete'),
    );
  }
  return $form;
}

/**
 * Submit handler for creating/editing lob_type.
 */
function lob_type_form_submit(&$form, &$form_state) {
  $lob_type = entity_ui_form_submit_build_entity($form, $form_state);
  // Save and go back.
  lob_type_save($lob_type);

  // Redirect user back to list of lob types.
  $form_state['redirect'] = 'admin/structure/lob-types';
}

/**
 *
 */
function lob_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/lob-types/' . $form_state['lob_type']->type . '/delete';
}

/**
 * Lob type delete form.
 */
function lob_type_form_delete_confirm($form, &$form_state, $lob_type) {
  $form_state['lob_type'] = $lob_type;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['lob_type_id'] = array(
    '#type' => 'value',
    '#value' => entity_id('lob_type', $lob_type),
  );
  return confirm_form($form,
    t('Are you sure you want to delete lob type %title?', array('%title' => entity_label('lob_type', $lob_type))),
    'lob/' . entity_id('lob_type', $lob_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Lob type delete form submit handler.
 */
function lob_type_form_delete_confirm_submit($form, &$form_state) {
  $lob_type = $form_state['lob_type'];
  lob_type_delete($lob_type);

  watchdog('lob_type', '@type: deleted %title.', array(
    '@type' => $lob_type->type,
    '%title' => $lob_type->label,
  ));
  drupal_set_message(t('@type %title has been deleted.', array(
    '@type' => $lob_type->type,
    '%title' => $lob_type->label,
  )));

  $form_state['redirect'] = 'admin/structure/lob-types';
}

/**
 * Page to select lob Type to add new lob.
 */
function lob_admin_add_page() {
  $items = array();
  foreach (lob_types() as $lob_type_key => $lob_type) {
    $items[] = l(entity_label('lob_type', $lob_type), 'lob/add/' . $lob_type_key);
  }
  return array(
    'list' => array(
      '#theme' => 'item_list',
      '#items' => $items,
      '#title' => t('Select type of lob to create.'),
    ),
  );
}

/**
 * Add new lob page callback.
 */
function lob_add($type) {
  $lob_type = lob_types($type);

  $lob = entity_create('lob', array('type' => $type));
  drupal_set_title(t('Create @name', array('@name' => entity_label('lob_type', $lob_type))));

  $output = drupal_get_form('lob_form', $lob);

  return $output;
}

/**
 * Lob Form.
 */
function lob_form($form, &$form_state, $lob) {
  $form_state['lob'] = $lob;

  $form['title'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Name'),
    '#default_value' => $lob->title,
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => $lob->description,
    '#required' => TRUE,
  );

  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $lob->uid,
  );

  field_attach_form('lob', $lob, $form, $form_state);

  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions'] = array(
    '#weight' => 100,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save lob'),
    '#submit' => $submit + array('lob_form_submit'),
  );

  // Show Delete button if we edit lob.
  $lob_id = entity_id('lob', $lob);
  if (!empty($lob_id) && lob_access('edit', $lob)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('lob_form_submit_delete'),
    );
  }

  $form['#validate'][] = 'lob_form_validate';

  return $form;
}

/**
 *
 */
function lob_form_validate($form, &$form_state) {

}

/**
 * Lob submit handler.
 */
function lob_form_submit($form, &$form_state) {

  $lob = $form_state['lob'];
  entity_form_submit_build_entity('lob', $lob, $form, $form_state);
  lob_save($lob);
  $lob_uri = entity_uri('lob', $lob);
  $form_state['redirect'] = $lob_uri['path'];
  if ($lob->type == 'address') {
    $address_data = $form_state['values']['main_address'][LANGUAGE_NONE][0];
    $address = lob_create_address($address_data);
    if (isset($address['id'])) {
      db_update('lob')
        ->fields(array('iid' => $address['id']))
        ->condition('lid', $lob->lid)
        ->execute();
    }
  }
  // If user choose Save & send , we create and send letter or postcard, else just save entity.
  if (isset($form_state['values']['store_way'][LANGUAGE_NONE][0]['value']) && $form_state['values']['store_way'][LANGUAGE_NONE][0]['value'] == 0) {
    if ($lob->type != 'address') {
      foreach ($form_state['values']['sender_address'][LANGUAGE_NONE][0] as $key => $value) {
        $sender_address[$key] = $value;
      }
      foreach ($form_state['values']['receiver_address'][LANGUAGE_NONE][0] as $key => $value) {
        $receiver_address[$key] = $value;
      }
      $lob_sender_address = lob_create_address($sender_address);
      $sender_address_id = $lob_sender_address['id'];
      $lob_reciever_address = lob_create_address($receiver_address);
      $receiver_address_id = $lob_reciever_address['id'];

      if ($lob->type == 'postcards') {
        $front_file = file_load($form_state['values']['front_file'][LANGUAGE_NONE][0]['fid']);
        $back_file = file_load($form_state['values']['back_file'][LANGUAGE_NONE][0]['fid']);
        $front_file_url = file_create_url($front_file->uri);
        $back_file_url = file_create_url($back_file->uri);
        if ($front_file->filemime == 'text/html') {
          $front_html = file_get_contents($front_file_url, FILE_USE_INCLUDE_PATH);
        }
        if ($back_file->filemime == 'text/html') {
          $back_html = file_get_contents($back_file_url, FILE_USE_INCLUDE_PATH);
        }
        $size = ($form_state['values']['size'][LANGUAGE_NONE][0]['value'] == 0) ? '4x6' : '6x11';
        $postcard_create = array(
          'description' => $lob->description,
          'to' => $receiver_address_id,
          'from' => $sender_address_id,
          'front' => !empty($front_html) ? $front_html : $front_file_url,
          'back' => !empty($back_html) ? $back_html : $back_file_url,
          'name' => $receiver_address['name_line'],
          'size' => $size,
        );
        $postcard = lob_create_postcard($postcard_create);
        if (!empty($postcard['tracking'])) {
          $date = $postcard['expected_delivery_date'];
          db_update('lob')
            ->fields(array(
              'tracking_id' => $postcard['tracking']['id'],
              'tracking_number' => $postcard['tracking']['tracking_number'],
              'expected_date' => $date,
              'iid' => $postcard['id'],
            ))
            ->condition('lid', $lob->lid)
            ->execute();
          drupal_set_message(check_plain(t('Postcard %title saved.', array('%title' => entity_label('lob', $lob)))));
          drupal_set_message(check_plain(t('Your tracking id is') . '&nbsp;' . $postcard['tracking']['id']));
          drupal_set_message(check_plain(t('Your tracking number is') . '&nbsp;' . $postcard['tracking']['tracking_number']));
        }
      }
      if ($lob->type == 'letters') {
        $color = $form_state['values']['color'][LANGUAGE_NONE][0]['value'] == 0 ? FALSE : TRUE;
        $letter_file = file_load($form_state['values']['letter_file'][LANGUAGE_NONE][0]['fid']);
        $letter_file_url = file_create_url($letter_file->uri);
        $sender_address['description'] = $lob->description;
        $sender_address['color'] = $color;
        $sender_address['path'] = $letter_file_url;
        $letter = lob_create_letter($sender_address, $receiver_address);
        if (isset($letter['tracking']) && !empty($letter['tracking'])) {
          $date = $letter['expected_delivery_date'];
          db_update('lob')
            ->fields(array(
              'tracking_id' => $letter['tracking']['id'],
              'tracking_number' => $letter['tracking']['tracking_number'],
              'expected_date' => $date,
              'iid' => $letter['id'],
            ))
            ->condition('lid', $lob->lid)
            ->execute();
          drupal_set_message(check_plain(t('Postcard %title saved.', array('%title' => entity_label('lob', $lob)))));
          drupal_set_message(check_plain(t('Your tracking id is') . '&nbsp;' . $letter['tracking']['id']));
          drupal_set_message(check_plain(t('Your tracking number is') . '&nbsp;' . $letter['tracking']['tracking_number']));
        }
      }
    }
  }
}

/**
 * Submit delete handler.
 *
 * @param $form
 * @param $form_state
 */
function lob_form_submit_delete($form, &$form_state) {
  $lob = $form_state['lob'];
  $lob_uri = entity_uri('lob', $lob);
  $form_state['redirect'] = $lob_uri['path'] . '/delete';
}

/**
 * Delete confirmation form.
 */
function lob_delete_form($form, &$form_state, $lob) {
  $form_state['lob'] = $lob;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['lob_type_id'] = array(
    '#type' => 'value',
    '#value' => entity_id('lob', $lob),
  );
  $lob_uri = entity_uri('lob', $lob);
  return confirm_form($form,
    t('Are you sure you want to delete lob %title?', array('%title' => entity_label('lob', $lob))),
    $lob_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function lob_delete_form_submit($form, &$form_state) {
  $lob = $form_state['lob'];
  lob_delete($lob);

  drupal_set_message(t('lob %title deleted.', array('%title' => entity_label('lob', $lob))));

  $form_state['redirect'] = '<front>';
}
