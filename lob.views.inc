<?php
/**
 * @file
 * Provide default view for entities of type lob.
 */

/**
 * Implements hook_views_default_views().
 */
function lob_views_default_views() {
  $view = new view();
  $view->name = 'postcards';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'lob';
  $view->human_name = 'Postcards';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Postcards';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'tracking_id' => 'tracking_id',
    'sender_address_data' => 'sender_address_data',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'tracking_id' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'sender_address_data' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Lob: Sender address: */
  $handler->display->display_options['fields']['sender_address']['id'] = 'sender_address';
  $handler->display->display_options['fields']['sender_address']['table'] = 'field_data_sender_address';
  $handler->display->display_options['fields']['sender_address']['field'] = 'sender_address';
  $handler->display->display_options['fields']['sender_address']['click_sort_column'] = 'country';
  $handler->display->display_options['fields']['sender_address']['settings'] = array(
    'use_widget_handlers' => 1,
    'format_handlers' => array(
      'address' => 'address',
    ),
  );
  /* Field: Lob: Receiver address: */
  $handler->display->display_options['fields']['receiver_address']['id'] = 'receiver_address';
  $handler->display->display_options['fields']['receiver_address']['table'] = 'field_data_receiver_address';
  $handler->display->display_options['fields']['receiver_address']['field'] = 'receiver_address';
  $handler->display->display_options['fields']['receiver_address']['click_sort_column'] = 'country';
  $handler->display->display_options['fields']['receiver_address']['settings'] = array(
    'use_widget_handlers' => 1,
    'format_handlers' => array(
      'address' => 'address',
    ),
  );
  /* Field: Lob: File front of postcard */
  $handler->display->display_options['fields']['front_file']['id'] = 'front_file';
  $handler->display->display_options['fields']['front_file']['table'] = 'field_data_front_file';
  $handler->display->display_options['fields']['front_file']['field'] = 'front_file';
  $handler->display->display_options['fields']['front_file']['click_sort_column'] = 'fid';
  /* Field: Lob: File back of postcard */
  $handler->display->display_options['fields']['back_file']['id'] = 'back_file';
  $handler->display->display_options['fields']['back_file']['table'] = 'field_data_back_file';
  $handler->display->display_options['fields']['back_file']['field'] = 'back_file';
  $handler->display->display_options['fields']['back_file']['click_sort_column'] = 'fid';
  /* Field: Lob: Tracking_id */
  $handler->display->display_options['fields']['tracking_id']['id'] = 'tracking_id';
  $handler->display->display_options['fields']['tracking_id']['table'] = 'lob';
  $handler->display->display_options['fields']['tracking_id']['field'] = 'tracking_id';
  $handler->display->display_options['fields']['tracking_id']['label'] = 'Tracking ID';
  /* Field: Lob: Tracking_number */
  $handler->display->display_options['fields']['tracking_number']['id'] = 'tracking_number';
  $handler->display->display_options['fields']['tracking_number']['table'] = 'lob';
  $handler->display->display_options['fields']['tracking_number']['field'] = 'tracking_number';
  $handler->display->display_options['fields']['tracking_number']['label'] = 'Tracking number';
  /* Field: Lob: Lob ID */
  $handler->display->display_options['fields']['lid']['id'] = 'lid';
  $handler->display->display_options['fields']['lid']['table'] = 'lob';
  $handler->display->display_options['fields']['lid']['field'] = 'lid';
  /* Filter criterion: Lob: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'lob';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'postcards' => 'postcards',
  );

  /* Display: Postcards */
  $handler = $view->new_display('page', 'Postcards', 'page');
  $handler->display->display_options['path'] = 'postcards';

  /* Display: Letters */
  $handler = $view->new_display('page', 'Letters', 'page_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Letters';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Lob: Sender address: */
  $handler->display->display_options['fields']['sender_address']['id'] = 'sender_address';
  $handler->display->display_options['fields']['sender_address']['table'] = 'field_data_sender_address';
  $handler->display->display_options['fields']['sender_address']['field'] = 'sender_address';
  $handler->display->display_options['fields']['sender_address']['click_sort_column'] = 'country';
  $handler->display->display_options['fields']['sender_address']['settings'] = array(
    'use_widget_handlers' => 1,
    'format_handlers' => array(
      'address' => 'address',
    ),
  );
  /* Field: Lob: Receiver address: */
  $handler->display->display_options['fields']['receiver_address']['id'] = 'receiver_address';
  $handler->display->display_options['fields']['receiver_address']['table'] = 'field_data_receiver_address';
  $handler->display->display_options['fields']['receiver_address']['field'] = 'receiver_address';
  $handler->display->display_options['fields']['receiver_address']['click_sort_column'] = 'country';
  $handler->display->display_options['fields']['receiver_address']['settings'] = array(
    'use_widget_handlers' => 1,
    'format_handlers' => array(
      'address' => 'address',
    ),
  );
  /* Field: Lob: Letter File */
  $handler->display->display_options['fields']['letter_file']['id'] = 'letter_file';
  $handler->display->display_options['fields']['letter_file']['table'] = 'field_data_letter_file';
  $handler->display->display_options['fields']['letter_file']['field'] = 'letter_file';
  $handler->display->display_options['fields']['letter_file']['click_sort_column'] = 'fid';
  /* Field: Lob: Tracking_id */
  $handler->display->display_options['fields']['tracking_id']['id'] = 'tracking_id';
  $handler->display->display_options['fields']['tracking_id']['table'] = 'lob';
  $handler->display->display_options['fields']['tracking_id']['field'] = 'tracking_id';
  $handler->display->display_options['fields']['tracking_id']['label'] = 'Tracking ID';
  /* Field: Lob: Tracking_number */
  $handler->display->display_options['fields']['tracking_number']['id'] = 'tracking_number';
  $handler->display->display_options['fields']['tracking_number']['table'] = 'lob';
  $handler->display->display_options['fields']['tracking_number']['field'] = 'tracking_number';
  $handler->display->display_options['fields']['tracking_number']['label'] = 'Tracking number';
  /* Field: Lob: Lob ID */
  $handler->display->display_options['fields']['lid']['id'] = 'lid';
  $handler->display->display_options['fields']['lid']['table'] = 'lob';
  $handler->display->display_options['fields']['lid']['field'] = 'lid';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Lob: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'lob';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'letters' => 'letters',
  );
  $handler->display->display_options['path'] = 'letters';

  /* Display: Addresses */
  $handler = $view->new_display('page', 'Addresses', 'page_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Addresses';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Lob: Address */
  $handler->display->display_options['fields']['main_address']['id'] = 'main_address';
  $handler->display->display_options['fields']['main_address']['table'] = 'field_data_main_address';
  $handler->display->display_options['fields']['main_address']['field'] = 'main_address';
  $handler->display->display_options['fields']['main_address']['click_sort_column'] = 'country';
  $handler->display->display_options['fields']['main_address']['settings'] = array(
    'use_widget_handlers' => 1,
    'format_handlers' => array(
      'address' => 'address',
    ),
  );
  /* Field: Lob: Iid */
  $handler->display->display_options['fields']['iid']['id'] = 'iid';
  $handler->display->display_options['fields']['iid']['table'] = 'lob';
  $handler->display->display_options['fields']['iid']['field'] = 'iid';
  $handler->display->display_options['fields']['iid']['label'] = 'Address ID';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Lob: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'lob';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'address' => 'address',
  );
  $handler->display->display_options['path'] = 'addresses';

  /* Display: Lob Items */
  $handler = $view->new_display('page', 'Lob Items', 'page_3');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Lob items';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'type',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['style_options']['columns'] = array(
    'type' => 'type',
    'title' => 'title',
    'lid' => 'lid',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'type' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'lid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Lob: Type */
  $handler->display->display_options['fields']['type']['id'] = 'type';
  $handler->display->display_options['fields']['type']['table'] = 'lob';
  $handler->display->display_options['fields']['type']['field'] = 'type';
  $handler->display->display_options['fields']['type']['exclude'] = TRUE;
  /* Field: Lob: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'lob';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  /* Field: Lob: Lob ID */
  $handler->display->display_options['fields']['lid']['id'] = 'lid';
  $handler->display->display_options['fields']['lid']['table'] = 'lob';
  $handler->display->display_options['fields']['lid']['field'] = 'lid';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Lob: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'lob';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'all' => 'all',
    'address' => 'address',
    'postcards' => 'postcards',
    'letters' => 'letters',
  );
  $handler->display->display_options['path'] = 'lob-items';

  $views[$view->name] = $view;
  return $views;
}
