<?php
/**
 * @file
 * Hooks provided by this module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Acts on lob being loaded from the database.
 *
 * This hook is invoked during $lob loading, which is handled by
 * entity_load(), via the EntityCRUDController.
 *
 * @param array $entities
 *   An array of $lob entities being loaded, keyed by id.
 *
 * @see hook_entity_load()
 */
function hook_lob_load(array $entities) {
  $result = db_query('SELECT pid, foo FROM {mytable} WHERE pid IN(:ids)', array(':ids' => array_keys($entities)));
  foreach ($result as $record) {
    $entities[$record->pid]->foo = $record->foo;
  }
}

/**
 * Responds when a $lob is inserted.
 *
 * This hook is invoked after the $lob is inserted into the database.
 *
 * @param Lob $lob
 *   The $lob that is being inserted.
 *
 * @see hook_entity_insert()
 */
function hook_lob_insert(Lob $lob) {
  db_insert('mytable')
    ->fields(array(
      'id' => entity_id('lob', $lob),
      'extra' => print_r($lob, TRUE),
    ))
    ->execute();
}

/**
 * Acts on a $lob being inserted or updated.
 *
 * This hook is invoked before the $lob is saved to the database.
 *
 * @param Lob $lob
 *   The $lob that is being inserted or updated.
 *
 * @see hook_entity_presave()
 */
function hook_lob_presave(Lob $lob) {
  $lob->name = 'foo';
}

/**
 * Responds to a $lob being updated.
 *
 * This hook is invoked after the $lob has been updated in the database.
 *
 * @param Lob $lob
 *   The $lob that is being updated.
 *
 * @see hook_entity_update()
 */
function hook_lob_update(Lob $lob) {
  db_update('mytable')
    ->fields(array('extra' => print_r($lob, TRUE)))
    ->condition('id', entity_id('lob', $lob))
    ->execute();
}

/**
 * Responds to $lob deletion.
 *
 * This hook is invoked after the $lob has been removed from the database.
 *
 * @param Lob $lob
 *   The $lob that is being deleted.
 *
 * @see hook_entity_delete()
 */
function hook_lob_delete(Lob $lob) {
  db_delete('mytable')
    ->condition('pid', entity_id('lob', $lob))
    ->execute();
}

/**
 * Act on a lob that is being assembled before rendering.
 *
 * @param object $lob
 *   The lob entity.
 * @param array $view_mode
 *   The view mode the lob is rendered in.
 * @param object $langcode
 *   The language code used for rendering.
 *
 *   The module may add elements to $lob->content prior to rendering. The
 *   structure of $lob->content is a renderable array as expected by
 *   drupal_render().
 *
 * @see hook_entity_prepare_view()
 * @see hook_entity_view()
 */
function hook_lob_view($lob, $view_mode, $langcode) {
  $additional_field = '';
  $lob->content['my_additional_field'] = array(
    '#markup' => $additional_field,
    '#weight' => 10,
    '#theme' => 'mymodule_my_additional_field',
  );
}

/**
 * Alter the results of entity_view() for lobs.
 *
 * @param array $build
 *   A renderable array representing the lob content.
 *
 *   This hook is called after the content has been assembled in a structured
 *   array and may be used for doing processing which requires that the complete
 *   lob content structure has been built.
 *
 *   If the module wishes to act on the rendered HTML of the lob rather than
 *   the structured content array, it may use this hook to add a #post_render
 *   callback. Alternatively, it could also implement hook_preprocess_lob().
 *   See drupal_render() and theme() documentation respectively for details.
 *
 * @see hook_entity_view_alter()
 */
function hook_lob_view_alter($build) {
  if ($build['#view_mode'] == 'full' && isset($build['an_additional_field'])) {
    // Change its weight.
    $build['an_additional_field']['#weight'] = -10;

    // Add a #post_render callback to act on the rendered HTML of the entity.
    $build['#post_render'][] = 'my_module_post_render';
  }
}

/**
 * Acts on lob_type being loaded from the database.
 *
 * This hook is invoked during lob_type loading, which is handled by
 * entity_load(), via the EntityCRUDController.
 *
 * @param array $entities
 *   An array of lob_type entities being loaded, keyed by id.
 *
 * @see hook_entity_load()
 */
function hook_lob_type_load(array $entities) {
  $result = db_query('SELECT pid, foo FROM {mytable} WHERE pid IN(:ids)', array(':ids' => array_keys($entities)));
  foreach ($result as $record) {
    $entities[$record->pid]->foo = $record->foo;
  }
}

/**
 * Responds when a lob_type is inserted.
 *
 * This hook is invoked after the lob_type is inserted into the database.
 *
 * @param LobType $lob_type
 *   The lob_type that is being inserted.
 *
 * @see hook_entity_insert()
 */
function hook_lob_type_insert(LobType $lob_type) {
  db_insert('mytable')
    ->fields(array(
      'id' => entity_id('lob_type', $lob_type),
      'extra' => print_r($lob_type, TRUE),
    ))
    ->execute();
}

/**
 * Acts on a lob_type being inserted or updated.
 *
 * This hook is invoked before the lob_type is saved to the database.
 *
 * @param LobType $lob_type
 *   The lob_type that is being inserted or updated.
 *
 * @see hook_entity_presave()
 */
function hook_lob_type_presave(LobType $lob_type) {
  $lob_type->name = 'foo';
}

/**
 * Responds to a lob_type being updated.
 *
 * This hook is invoked after the lob_type has been updated in the database.
 *
 * @param LobType $lob_type
 *   The lob_type that is being updated.
 *
 * @see hook_entity_update()
 */
function hook_lob_type_update(LobType $lob_type) {
  db_update('mytable')
    ->fields(array('extra' => print_r($lob_type, TRUE)))
    ->condition('id', entity_id('lob_type', $lob_type))
    ->execute();
}

/**
 * Responds to lob_type deletion.
 *
 * This hook is invoked after the lob_type has been removed from the database.
 *
 * @param LobType $lob_type
 *   The lob_type that is being deleted.
 *
 * @see hook_entity_delete()
 */
function hook_lob_type_delete(LobType $lob_type) {
  db_delete('mytable')
    ->condition('pid', entity_id('lob_type', $lob_type))
    ->execute();
}

/**
 * Define default lob_type configurations.
 *
 * @return array
 *   An array of default lob_type, keyed by machine names.
 *
 * @see hook_default_lob_type_alter()
 */
function hook_default_lob_type() {
  $defaults['main'] = entity_create('lob_type', array());
  return $defaults;
}

/**
 * Alter default lob_type configurations.
 *
 * @param array $defaults
 *   An array of default lob_type, keyed by machine names.
 *
 * @see hook_default_lob_type()
 */
function hook_default_lob_type_alter(array &$defaults) {
  $defaults['main']->name = 'custom name';
}
