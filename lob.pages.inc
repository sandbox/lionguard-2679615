<?php
/**
 * @file
 * Provide callback for entity page view.
 */

/**
 * Task view callback.
 */
function lob_view($lob) {
  drupal_set_title(entity_label('lob', $lob));
  return entity_view('lob', array(entity_id('lob', $lob) => $lob), 'full');
}
